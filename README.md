# Dotfiles


## Suggestions for a good README

## Name

My Dotfiles

## Description

## Applications Used

* Window Manager: i3-gaps
* Notification: dunst
* Compositer: Picom
* Bar: polybar
* Terminal : alacritty
* Shell: zsh (oh-my-zsh)
* Pdf Viewer: zathura
* File Broswer: ranger
* Rss: newsboat
* Mail Client: Thunderbird


## Screenshot

![screenshot](image/screenshot1.png)

## Authors and acknowledgment

## License

## Project status

